open Images;
open LeagueButtons;
open Types;

let str = React.string;

[@react.component]
let make = /*props*/
    (
      ~navToLeague: string => unit,
      ~onScroll: (int, unit) => unit,
      ~scrollTop: int,
    ) => {
  //---------------------------------------------
  //---------------------------------------layout
  <div id="HomePage">
    <Header boxShadow="#141619 0px 0px 51px 5px">
      {"Select a league" |> str}
    </Header>
    <ScrollView id="ScrollView_Home" onScroll padding="0" scrollTop>
      <main className="content_Home">
        {leagueButtons
         |> Array.map((button: leagueButton)
              //alignItems="flex-start"
              =>
                <Button
                  background="#333"
                  border="solid #222"
                  borderRadius="0"
                  borderWidth="0 0px 5px"
                  className={button.name}
                  id={button.name ++ button.id}
                  justifyContent="flex-start"
                  key={button.name ++ button.id}
                  onClick={() =>
                    /*navigate to the league page using the id*/ navToLeague(
                      button.id,
                    )
                  }
                  padding="1.5rem 2.5rem"
                  width="100%">
                  <Box
                    alignItems="flex-start"
                    flex="0.8"
                    flexDirection="column"
                    margin="0">
                    <h3
                      style={ReactDOMRe.Style.make(
                        ~color="inherit",
                        ~textAlign="left",
                        (),
                      )}>
                      {button.name |> str}
                    </h3>
                    <p
                      style={ReactDOMRe.Style.make(
                        ~color="#ccf",
                        ~margin="0.4rem 0",
                        (),
                      )}>
                      {"Football" |> str}
                    </p>
                    <p
                      style={ReactDOMRe.Style.make(
                        ~color="#cc8",
                        ~margin="0.4rem 0",
                        (),
                      )}>
                      {button.country |> str}
                    </p>
                  </Box>
                  /*for incomplete league data*/
                  /*for now, only NWSL is incomplete, id = 4521*/
                  {button.id == nwsl.id
                     ? <p
                         style={ReactDOMRe.Style.make(
                           ~bottom="0.5rem",
                           ~color="lightpink",
                           ~position="absolute",
                           ~right="2rem",
                           (),
                         )}>
                         {"Incomplete data" |> str}
                       </p>
                     : React.null}
                </Button>
              )
         |> React.array}
      </main>
      //------------------------------------------------
      //----------------------------------------footer
      <Footer padding="2.5rem 9% 2rem" position="static">
        <p className="inlineB">
          {"Data for this app comes from " |> str}
          <a href="https://www.thesportsdb.com/" target="_blank">
            {"TheSportsDB" |> str}
          </a>
          {", a cool database for us sports fanatics. Check it out and support them on patreon. There's so much data available!"
           |> str}
        </p>
        <p
          style={ReactDOMRe.Style.make(
            ~margin="3rem 0 0",
            ~textAlign="center",
            (),
          )}>
          {"This app was crafted by Darren, the amateur programming god" |> str}
        </p>
        <img
          alt="flag_of_cameroon"
          src=cameroon
          style={ReactDOMRe.Style.make(~padding="1.5rem", ())}
        />
        <span style={ReactDOMRe.Style.make(~margin="4rem 0 0", ())}>
          {{js|© |js} ++ {|2019|} ++ " D2C Labs" |> str}
        </span>
      </Footer>
    </ScrollView>
    <div />
  </div>;
};
