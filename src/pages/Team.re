open Decoder;
open Fallback;
open Helpers;
open Types;

type state = {
  currentTeam: team,
  last5: list(result),
  loadingTeam: bool,
  next5: list(fixture),
  showDescription: bool,
};

type action =
  | LoadLast5(list(result))
  | LoadNext5(list(fixture))
  | LoadTeam(list(team))
  | ReloadTeam
  | Toggle(tab, bool);

[@react.component]
let make = /*props*/
    (
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~resetScrollTop: unit => unit,
      ~scrollTop: int=0,
      ~teamIdProp: string,
      ~urlHash: string,
    ) => {
  //------------------------------------------
  //--------------------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | LoadLast5(results) => {...state, last5: results}
        | LoadNext5(fixtures) => {...state, next5: fixtures}
        | LoadTeam(response) => {
            ...state,
            currentTeam:
              response
              |> Array.of_list
              |> (/*get the 1st item in the array*/ array => array[0]),
            loadingTeam: false,
          }
        | ReloadTeam => {...state, loadingTeam: true}
        | Toggle(tab, bool) =>
          switch (tab) {
          | TeamBio => {...state, showDescription: !bool}
          | _ => state
          }
        },
      //initial state
      {
        currentTeam: defaultTeam,
        last5: [],
        loadingTeam: true,
        next5: [],
        showDescription: false,
      },
    );
  //----------------------------------------
  //--------------------------end of reducer

  //shorthand
  let team: team = state.currentTeam;

  //load team details
  let loadTeamDetails: string => unit =
    someTeamId => {
      //get last 5 fixture/match results
      getLast5(someTeamId)
      |> Js.Promise.then_((response: list(result)) => {
           //send to state
           dispatch(LoadLast5(response));
           Js.Promise.resolve();
         })
      |> ignore;

      //get next 5 fixtures/matches
      getNext5(someTeamId)
      |> Js.Promise.then_((response: list(fixture)) => {
           //send to state
           dispatch(LoadNext5(response));
           Js.Promise.resolve();
         })
      |> ignore;

      getTeamBio(someTeamId)
      |> Js.Promise.then_((response: list(team)) => {
           //send to state
           dispatch(LoadTeam(response));
           Js.Promise.resolve();
         })
      |> ignore;
    };

  let _onRender =
    React.useEffect0(() => {
      resetScrollTop();
      loadTeamDetails(teamIdProp);
      None;
    });

  //----------------------------------------
  //----------------------------------layout
  state.loadingTeam == true
    ? <Loader text="Fetching team data..." />
    : <div id="TeamPage">
        <Header>
          <div className="leagueName">
            {(team.leagueId |> idToLeagueName) ++ ": " |> str}
          </div>
          <div className="teamName greenText"> {team.name |> str} </div>
        </Header>
        {switch (urlHash) {
         | "last5" => <Results results={state.last5} team />
         | "next5" => <Fixtures fixtures={state.next5} team />
         | "about"
         | _ =>
           <TeamBio
             click_Description={() =>
               dispatch(Toggle(TeamBio, state.showDescription))
             }
             onScroll
             scrollTop
             show_Description={state.showDescription}
             team
           />
         }}
        //-----------------
        //footer/navigation
        <Navigation team />
      </div>;
};
