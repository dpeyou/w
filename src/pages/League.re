open Decoder;
open Fallback;
open Helpers;
open Types;

type state = {
  currentLeague: league,
  fixtures: list(fixture),
  loadingLeague: bool,
  results: list(result),
  scrollTop_Bio: int,
  scrollTop_Fixtures: int,
  scrollTop_Results: int,
  scrollTop_Table: int,
  table: list(ranking),
};

type action =
  | LoadFixtures(list(fixture))
  | LoadLeague(list(league))
  | LoadResults(list(result))
  | LoadTable(list(ranking))
  | SetScrollTop(tab, int);

[@react.component]
let make = /*props*/
    (
      ~currentSeason: string,
      ~leagueIdProp: string,
      ~navToTeam: (string, unit) => unit,
      ~urlHash: string,
    ) => {
  //-------------------------------------------
  //---------------------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (state, action) =>
        switch (action) {
        | LoadFixtures(response) => {...state, fixtures: response}
        | LoadLeague(response) => {
            ...state,
            currentLeague: response |> Array.of_list |> (array => array[0]),
            loadingLeague: false,
          }
        | LoadResults(response) => {...state, results: response}
        | LoadTable(rankings) => {...state, table: rankings}

        | SetScrollTop(tab, scrollTop) =>
          switch (tab) {
          | Fixtures => {...state, scrollTop_Fixtures: scrollTop}
          | LeagueBio => {...state, scrollTop_Bio: scrollTop}
          | Results => {...state, scrollTop_Results: scrollTop}
          | Table => {...state, scrollTop_Table: scrollTop}
          | _ => state
          }
        },
      //initial state
      {
        currentLeague: defaultLeague,
        fixtures: [],
        loadingLeague: true,
        results: [],
        scrollTop_Bio: 0,
        scrollTop_Fixtures: 0,
        scrollTop_Results: 0,
        scrollTop_Table: 0,
        table: [],
      },
    );
  //-------------------------------------------
  //-----------------------------end of reducer

  let loadLeagueDetails: string => unit =
    someLeagueId => {
      //get league table
      getLeagueTable(someLeagueId, currentSeason)
      |> Js.Promise.then_((response: list(ranking)) => {
           //send to state
           dispatch(LoadTable(response));
           Js.Promise.resolve();
         })
      |> ignore;

      //get basic league bio
      getLeagueBio(someLeagueId)
      |> Js.Promise.then_((response: list(league)) => {
           dispatch(LoadLeague(response));
           Js.Promise.resolve();
         })
      |> ignore;

      //get next fixtures
      getLeagueSchedule(someLeagueId)
      |> Js.Promise.then_((response: list(fixture)) => {
           dispatch(LoadFixtures(response));
           Js.Promise.resolve();
         })
      |> ignore;

      //get results of previous fixtures
      getLeagueResults(someLeagueId)
      |> Js.Promise.then_((response: list(result)) => {
           dispatch(LoadResults(response));
           Js.Promise.resolve();
         })
      |> ignore;
    };

  let _onRender =
    React.useEffect0(() => {
      loadLeagueDetails(leagueIdProp);
      None;
    });

  //shorthand
  let currentTab: tab = urlHash |> hashToTab;
  let league: league = state.currentLeague;

  //----------------------------------------
  //----------------------------------layout
  <div id="LeaguePage">
    <Header logo={league.logo}> {league.name |> str} </Header>
    {switch (currentTab) {
     | LeagueBio =>
       <LeagueBio
         league
         onScroll={(scrollTop, ()) =>
           dispatch(SetScrollTop(LeagueBio, scrollTop))
         }
         scrollTop={state.scrollTop_Bio}
       />
     | Fixtures =>
       <Fixtures
         clickable=true
         clickTeam={teamId => navToTeam(teamId)}
         fixtures={state.fixtures}
         onScroll={(scrollTop, ()) =>
           dispatch(SetScrollTop(Fixtures, scrollTop))
         }
         scrollTop={state.scrollTop_Fixtures}
         text="Kickoff is in US pacific time (UTC-07). Click on a team to navigate to it"
       />

     | Results =>
       <Results
         clickable=true
         clickTeam={teamId => navToTeam(teamId)}
         onScroll={(scrollTop, ()) =>
           dispatch(SetScrollTop(Results, scrollTop))
         }
         results={state.results}
         scrollTop={state.scrollTop_Results}
         text="Most recent matchday results. Click on a team to navigate to it"
       />

     | Table =>
       <Table
         clickTeam={(ranking, ()) =>
           push("/team/" ++ ranking.teamId ++ "#next5")
         }
         league
         onScroll={(scrollTop, ()) =>
           dispatch(SetScrollTop(Table, scrollTop))
         }
         scrollTop={state.scrollTop_Table}
         table={state.table}
       />
     | _ => <Loader text="reading url..." />
     //-----------------
     //footer/navigation
     }}
    <Navigation leagueId={league.id} />
    <ScrollToTop
      currentPage={League(league.id, urlHash)}
      display={
        (
          switch (currentTab) {
          | Fixtures => state.scrollTop_Fixtures
          | LeagueBio => state.scrollTop_Bio
          | Results => state.scrollTop_Results
          | Table => state.scrollTop_Table
          | _ => 0
          }
        )
        > 600
          ? "block" : "none"
      }
    />
  </div>;
};
