open Types;

[@react.component]
let make = /*props*/
    (
      ~click_Description: unit => unit,
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~scrollTop: int=0,
      ~show_Description: bool=false,
      ~team: team,
    ) => {
  //----------------------------------------
  //----------------------------------layout
  <ScrollView boxShadow="none" id="ScrollView_TeamBio" onScroll scrollTop>
    //---------
    //team logo

      {switch (team.badge) {
       | Some("") => React.null
       | Some(badge) =>
         <img
           alt="team_badge"
           src=badge
           style={ReactDOMRe.Style.make(
             ~background="linear-gradient(to bottom, #222, #334)",
             ~borderBottom="5px",
             ~padding="2.0rem 5% 1.3rem",
             ~width="90%",
             (),
           )}
         />
       | None => React.null /*or return a div that says "unable to load team badge/logo"*/
       }}
      //-------
      //fan art
      {switch (team.banner) {
       | Some("") => React.null
       | Some(banner) =>
         <img
           alt="team_art"
           src=banner
           style={ReactDOMRe.Style.make(
             ~borderBottom="5px solid rgb(34, 34, 51)",
             ~width="100%",
             (),
           )}
         />
       | None => React.null
       }}
      //------------------
      //social media links
      <MediaLinks team />
      //------------
      //about league
      {switch (team.description) {
       | Some("") => React.null
       | Some(description) =>
         <Collapsible
           buttonText_Closed="About club"
           buttonText_Opened="About club"
           className="teamBio"
           onClick=click_Description
           margin="1.5rem auto"
           showContent=show_Description
           text=description
           width="88%"
         />
       | None => React.null
       }}
    </ScrollView>;
};
