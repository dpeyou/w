//this component is similar to the <Collapsible/> component
open Fallback;
open Helpers;
open Types;

type state = {showScore: bool};

type action =
  | ToggleScore(bool);

[@react.component]
let make = /*props*/
    (
      ~clickable: bool=false /*Decide on whether we can navigate to teams we click on*/ /*Clickable on League page but not on Team page*/,
      ~clickTeam: (string, unit) => unit=(_teamId, ()) => (),
      ~result: result,
      ~team: team=defaultTeam,
    ) => {
  //-----------------------------------
  //-------------------start of reducer

  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | ToggleScore(showScore) => {showScore: !showScore}
        },
      //initial state
      {showScore: false},
    );
  //-----------------------------------
  //---------------------end of reducer

  //shorthand
  let showScore: bool = state.showScore;

  //-------------------------------
  //element for away and home teams
  let teamElement: (string, string, string) => React.element =
    (id, name, score) =>
      <Button
        background={
          /*change background depending on whether it is clickable, to make it look like a button or not*/ clickable
            ? "linear-gradient(to left, rgb(85, 90, 96), rgb(98, 96, 106)"
            : "none"
        }
        className="team"
        color={
          clickable
            ? "#ffe0a0"
            : {
              id == team.id ? "#dfd" : "#ffa0a0";
            }
        }
        onClick={clickable ? clickTeam(id) : nothingHappens}
        padding={clickable ? "0.3rem 0.6rem" : "0"}
        textAlign="left">
        <Box flex="1"> {name |> str} </Box>
        <Box color="#eee" whiteSpace="pre"> {" - " ++ score |> str} </Box>
      </Button>;

  //----------------------------------
  //----------------------------layout
  <div className="result">
    //------------
    //visible area

      <Box
        background="rgb(74, 74, 85)"
        borderRadius="3px"
        className="resultInfo"
        key={"result" ++ result.awayTeamId ++ result.homeTeamId ++ result.date}
        margin="1rem 0 0"
        minHeight="6rem"
        padding="1.25rem">
        //--------------------------------------
        //start of awayTeam & homeTeam container

          <Box
            alignItems="flex-start"
            className="box_Teams"
            flex="1.0"
            flexDirection="column"
            justifyContent="space-between"
            margin="0"
            overflowY="hidden">
            //---------
            //away team

              {teamElement(
                 result.awayTeamId,
                 result.awayTeamName,
                 result.awayTeamScore,
               )}
              <Box
                margin="0.25rem 0" whiteSpace={clickable ? "pre" : "initial"}>
                {" at " |> str}
              </Box>
              //---------
              //home team
              {teamElement(
                 result.homeTeamId,
                 result.homeTeamName,
                 result.homeTeamScore,
               )}
            </Box>
          //------------------------------------
          //end of awayTeam & homeTeam container
          //---------------------------------------------------------------
          //return W, D or L - based on whether the current team won, drew or lost. Only show if currentTeam !== defaultTeam
          <Box
            alignItems="flex-end"
            className="box_win_loss_draw_date"
            flex="0.5"
            flexDirection="column"
            //height="100%"
            margin="0"
            overflowY="hidden">
            {team == defaultTeam
               ? React.null
               : <div className="win_loss_draw yellowText">
                   {{{result.awayTeamScore > result.homeTeamScore
                        ? /*winner == away team*/ result.awayTeamName
                        : result.awayTeamScore < result.homeTeamScore
                            ? /*winner == home team*/ result.homeTeamName
                            : result.awayTeamScore == result.homeTeamScore
                                ? /*winner == draw*/ "draw" : ""}
                     |> (
                       /*check if the winner is the current team we are viewing (current team that is mapped)*/
                       winner =>
                         winner == team.name
                           ? "Win" : winner == "draw" ? "Draw" : "Loss"
                     )}
                    |> str}
                 </div>}
            //----
            //date
            <div className="resultDate"> {result.date |> str} </div>
            //---------
            //box score
            <Button
              background="linear-gradient(to left, rgb(85, 90, 96), rgb(98, 96, 106)"
              className="resultDate lightBlueText"
              onClick={() =>
                /*trigger hidden/collapsible content*/ dispatch(
                  ToggleScore(showScore),
                )
              }
              margin="0"
              padding="0.25rem 0.5rem">
              {"box score" |> str}
            </Button>
          </Box>
        </Box>
      //--------------------
      //end of visible area
      //----------------------------------
      //box score, hidden area/collapsible
      <Box
        alignItems="flex-start"
        background="rgba(53, 50, 59, 0.2)"
        border="solid #eee"
        borderWidth="0 0 0 3px"
        boxShadow="0 15px 38px -27px inset black"
        className="boxScore"
        flexDirection="column"
        maxHeight={showScore ? "500vh" : "0"}
        opacity={showScore ? "1" : "0"}
        padding={
          showScore
            ? "2rem"
            : /*hide paadding bcuz it occupies space even if content is hidden*/ "0"
        }
        pointerEvents={showScore ? "auto" : "none"}
        transition="200ms">
        <div
          className={
            clickable
              ? "awayTeam"
              : {
                team.id == result.awayTeamId
                  ? "awayTeam greenText" : "awayTeam redText";
              }
          }>
          {result.awayTeamName ++ ": " |> str}
          {/*check if zero goals were scored*/ result.awayTeamScore == "0"
             ? "LOL" |> str
             : {
               result.awayTeamScore_Details |> str;
             }}
        </div>
        <div
          className={
            clickable
              ? "homeTeam"
              : {
                team.id == result.homeTeamId
                  ? "homeTeam greenText" : "homeTeam redText";
              }
          }>
          {result.homeTeamName ++ ": " |> str}
          {/*check if zero goals were scored*/ result.homeTeamScore == "0"
             ? "LOL" |> str
             : {
               result.homeTeamScore_Details |> str;
             }}
        </div>
      </Box>
    </div>;
};
