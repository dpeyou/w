//"inherit" is a reserved keyword & messes up the syntax highighting in Atom editor. For fontFamily prop. As of June 2019
let _inherit = "inherit";

[@react.component]
let make = /*props*/
    (
      ~alignItems: string="center",
      ~alignSelf: string="",
      ~background: string="",
      ~border: string="",
      ~borderRadius: string="",
      ~borderWidth: string="",
      ~bottom: string="",
      ~boxShadow: string="none",
      ~color: string="",
      ~className: string="",
      ~display: string="flex",
      ~flex: string="",
      ~flexDirection: string="initial",
      ~fontFamily: string=_inherit,
      ~fontSize: string="",
      ~height: string="initial",
      ~id: string="",
      ~justifyContent: string="space-between",
      ~left: string="0",
      ~margin: string="0 auto",
      ~maxHeight: string="",
      ~minHeight: string="",
      ~onClick: ReactEvent.Mouse.t => unit=_event => (),
      ~opacity: string="1",
      ~overflowX: string="hidden",
      ~overflowY: string="auto",
      ~padding: string="",
      ~pointerEvents: string="auto",
      ~position: string="",
      ~right: string="0",
      ~textAlign: string="",
      ~top: string="",
      ~transition: string="400ms ease" /*cubic-bezier(0.08, 0.68, 0, 0.85)*/,
      ~whiteSpace: string="",
      ~width: string="",
      ~zIndex: string="",
      ~children: React.element,
    ) => {
  //layout
  <div
    className={className ++ " box"}
    id
    onClick
    style={ReactDOMRe.Style.make(
      ~alignItems,
      ~alignSelf,
      ~background,
      ~border,
      ~borderRadius,
      ~borderWidth,
      ~bottom,
      ~boxShadow,
      ~color,
      ~display,
      ~flex,
      ~flexDirection,
      ~fontFamily,
      ~fontSize,
      ~height,
      ~justifyContent,
      ~left,
      ~margin,
      ~maxHeight,
      ~minHeight,
      ~opacity,
      ~overflowX,
      ~overflowY,
      ~padding,
      ~pointerEvents,
      ~position,
      ~right,
      ~textAlign,
      ~top,
      ~transition,
      ~whiteSpace,
      ~width,
      ~zIndex,
      (),
    )}>
    children
  </div>;
};
