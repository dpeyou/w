open Fallback;
open Helpers;
open Types;

[@react.component]
let make = /*props*/
    (
      ~clickable: bool=false /*Decide on whether we can navigate to teams we click on*/ /*Clickable on League page but not on Team page*/,
      ~clickTeam: (string, unit) => unit=(_teamId, ()) => (),
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~results: list(result),
      ~scrollTop: int=0,
      ~team: team=defaultTeam,
      ~text: string="Results",
    ) => {
  //----------------------------------
  //----------------------------layout
  <ScrollView
    id={
      /*alternate ids based on the current page*/
      /*check whether a team was set*/ team == defaultTeam
        ? "ScrollView_Results" : "ScrollView_Last5"
    }
    onScroll
    scrollTop>
    <p
      className="text"
      style={ReactDOMRe.Style.make(
        ~color="lightyellow",
        ~margin="2.5rem 2rem 2rem",
        ~textAlign="center",
        (),
      )}>
      {text |> str}
    </p>
    {results
     |> Array.of_list
     |> Array.map((result: result) =>
          <Result
            clickable
            clickTeam
            key={result.awayTeamId ++ result.homeTeamId ++ result.date}
            result
            team
          />
        )
     |> React.array}
  </ScrollView>;
};
