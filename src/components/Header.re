let str = React.string;

[@react.component]
let make = /*~props*/
    (
      ~background: string="rgba(50, 50, 60, 0.95)",
      ~boxShadow: string="#060713 0px 0px 44px 4px",
      ~color: string="",
      ~height: string="3rem",
      ~logo: string="",
      ~position: string="absolute",
      ~children: React.element=React.null,
    ) => {
  //------------------------------------
  //------------------------------layout
  <header
    id="Header"
    style={ReactDOMRe.Style.make(
      //~alignItems="center",
      ~background,
      ~boxShadow,
      ~color,
      ~display="flex",
      ~height,
      ~left="0",
      //~paddingBottom="0.75rem",
      ~position,
      ~right="0",
      ~top="0",
      ~whiteSpace="pre",
      ~zIndex="3",
      (),
    )}>
    <Box justifyContent="center" margin="0 auto" padding="0 5%">
      children
      {logo == /*check for empty string*/ ""
         ? /*return nothing*/ React.null
         : <img
             alt="logo"
             src={
               logo ++ "/preview" /*adding "/preview" so that a less heavy image is fetched*/
             }
             style={ReactDOMRe.Style.make(
               ~borderRadius="30%",
               ~marginLeft="0.5rem",
               ~width="1.8rem",
               (),
             )}
           />}
    </Box>
  </header>;
};
