open Helpers;
open Types;

[@react.component]
let make =
    (
      ~background: string="rgba(77, 73, 83, 1.0)",
      ~border: string="#eee dashed",
      ~borderWidth: string="3px",
      ~borderRadius: string="10%",
      ~bottom: string="7rem",
      ~boxShadow: string="rgb(38, 40, 49) 0px 0px 25px 0px",
      ~color: string="#eee",
      ~currentPage: page,
      ~display: string="none",
    ) => {
  //-----------------------
  //-----------------layout
  <Button
    background
    border
    borderWidth
    borderRadius
    bottom
    boxShadow
    className="button_ScrollToTop"
    color
    display
    onClick={() => currentPage |> pageToScrollViewId |> scrollToTop}
    padding="0.9rem"
    position="absolute"
    right="5%"
    zIndex="1">
    {"top" |> str}
  </Button>;
};
