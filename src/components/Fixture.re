open Fallback;
open Helpers;
open Types;

[@react.component]
let make = /*props*/
    (
      ~clickable: bool=false /*Decide on whether we can navigate to teams we click on*/ /*Clickable on League page but not on Team page*/,
      ~clickTeam: (string, unit) => unit=(_teamId, ()) => (),
      ~fixture: fixture,
      ~team: team=defaultTeam,
    ) => {
  //-------------------------------
  //element for away and home teams
  let teamElement: (string, string) => React.element =
    (id, name) =>
      <Button
        background={
          /*change background depending on whether it is clickable, to make it look like a button or not*/
          clickable
            ? "linear-gradient(to left, rgb(85, 88, 96), rgb(95, 98, 106)"
            : "none"
        }
        className="team"
        color={
          clickable
            ? "#ffe0a0"
            : {
              id == team.id
                ? /*currentTeam's color, greenish*/ "#dfd"
                : /*other teams color, reddish*/ "#ffa0a0";
            }
        }
        onClick={clickable ? clickTeam(id) : nothingHappens}
        padding={clickable ? "0.3rem 0.6rem" : "0"}
        textAlign="left">
        {name |> str}
      </Button>;

  //----------------------------------------
  //----------------------------------layout
  <Box
    background="rgb(74, 74, 85)"
    borderRadius="3px"
    className="fixture"
    key={"fixture" ++ fixture.awayTeamId ++ fixture.homeTeamId ++ fixture.date}
    margin="1rem 0"
    minHeight="6rem"
    padding="1.25rem">
    //--------------------------------------
    //start of awayTeam & homeTeam container

      <Box
        alignItems="flex-start"
        className="box_Teams"
        flex="1.0"
        flexDirection="column"
        //height="100%"
        margin="0"
        overflowY="hidden"
        whiteSpace="pre">
        //---------
        //away team

          {teamElement(fixture.awayTeamId, fixture.awayTeamName)}
          <Box margin="0.25rem 0" whiteSpace={clickable ? "pre" : "initial"}>
            {" at " |> str}
          </Box>
          //---------
          //home team
          {teamElement(fixture.homeTeamId, fixture.homeTeamName)}
        </Box>
      //------------------------------------
      //end of awayTeam & homeTeam container
      //---------------------
      //time & date container
      <Box
        alignItems="flex-end"
        className="box_Time&Date"
        flex="0.5"
        flexDirection="column"
        // height="100%"
        justifyContent="space-between"
        margin="0">
        <div className="fixtureTime yellowText">
          {fixture.kickoff |> toPacificTime |> str}
        </div>
        <div className="fixtureDate"> {fixture.date |> str} </div>
        /*code below is for the <Team /> page only*/
        {/*check whether */ fixture.homeTeamId == fixture.awayTeamId
           ? React.null : <div className="competition blueText" />}
      </Box>
    </Box>;
};
