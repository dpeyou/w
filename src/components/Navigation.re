open Fallback;
open Helpers;
open Types;

[@react.component]
let make = /*props*/
    (
      ~height: string="5.2rem",
      ~leagueId: string=defaultLeague.id,
      ~team: team=defaultTeam,
    ) => {
  //get the current page
  let currentPage: page = ReasonReact.Router.useUrl() |> urlToPage;

  //for navigation within a page
  let currentTab: string =
    switch (currentPage) {
    | League(_, urlHash) => urlHash
    | Team(_, urlHash) => urlHash
    | _ => ""
    };

  //----------------------------------------------
  //triggers, to show active section within a page
  let showFixtures: bool = currentTab == "fixtures" ? true : false;
  let showLast5: bool = currentTab == "last5" ? true : false;
  let showLeagueBio: bool = currentTab == "leagueBio" ? true : false;
  let showNext5: bool = currentTab == "next5" ? true : false;
  let showResults: bool = currentTab == "results" ? true : false;
  let showTable: bool = currentTab == "table" ? true : false;
  let showTeamBio: bool = currentTab == "teamBio" ? true : false;

  //-------------------------------------------
  //buttons that switch pages, to home & league
  let button_Home: React.element =
    <Button
      background="rgb(105, 35, 35)"
      border="solid #333"
      borderWidth="3px 2px 0"
      borderRadius="0"
      flex="1"
      height="100%"
      onClick={() => push("/home/")}
      width="100%">
      {"Home" |> str}
    </Button>;

  let button_League: React.element =
    <Button
      background="rgba(50, 50, 60, 0.95)"
      border="solid rgb(51, 51, 51)"
      borderRadius="0"
      borderWidth="3px 2px 0"
      color="lightyellow"
      flex="1"
      height="100%"
      onClick={() => push("/league/" ++ team.leagueId ++ "#table")}
      width="100%">
      {"Back to league" |> str}
    </Button>;

  //---------------------------------------
  //buttons that switch sections/tabs, not pages
  //-----------------
  //team page buttons
  let button_TeamBio: menuButton = {
    onClick: () => push("/team/" ++ team.id ++ "#teamBio"),
    text: "About",
    trigger: showTeamBio,
  };

  let button_ShowLast5 = {
    onClick: () => push("/team/" ++ team.id ++ "#last5"),
    text: "Last 5",
    trigger: showLast5,
  };

  let button_ShowNext5 = {
    onClick: () => push("/team/" ++ team.id ++ "#next5"),
    text: "Next 5",
    trigger: showNext5,
  };

  //-------------------
  //league page buttons
  let button_LeagueBio = {
    onClick: () => push("/league/" ++ leagueId ++ "#leagueBio"),
    text: "About",
    trigger: showLeagueBio,
  };

  let button_ShowFixtures = {
    onClick: () => push("/league/" ++ leagueId ++ "#fixtures"),
    text: "Next",
    trigger: showFixtures,
  };

  let button_ShowResults = {
    onClick: () => push("/league/" ++ leagueId ++ "#results"),
    text: "Results",
    trigger: showResults,
  };

  let button_ShowTable = {
    onClick: () => push("/league/" ++ leagueId ++ "#table"),
    text: "Table",
    trigger: showTable,
  };

  //-----------------------------------------
  //-----------------------------------layout
  <Box
    bottom="0"
    className="footer"
    flexDirection="column"
    height
    id="Navigation"
    position="absolute"
    zIndex="2">
    //-----------
    //top buttons

      <Box
        bottom="2.6rem"
        className="container_TopButtons"
        height="2.6rem"
        justifyContent="center"
        position="absolute">
        //------------------------------------
        //change top buttons depending on page

          {(
             switch (currentPage) {
             | League(_) => [|
                 button_ShowFixtures,
                 button_ShowResults,
                 button_ShowTable,
                 button_LeagueBio,
               |]
             | Team(_) => [|
                 button_ShowNext5,
                 button_ShowLast5,
                 button_TeamBio,
               |]
             | _AnyOtherPage => [||]
             }
           )
           |> Array.map((button: menuButton) =>
                <Button
                  background={
                    button.trigger == true
                      ? "#ffa0a0" : "rgba(84, 80, 87, 1.0)"
                  }
                  border="solid #333"
                  borderWidth="3px 2px 0"
                  borderRadius="0"
                  color={button.trigger ? "#555" : "#eef"}
                  flex="1"
                  height="100%"
                  key={button.text}
                  onClick={button.onClick}>
                  {button.text |> str}
                </Button>
              )
           |> React.array}
        </Box>
      //-------------
      //lower buttons
      <Box
        bottom="0"
        className="container_LowerButtons"
        height="2.6rem"
        position="absolute">
        //--------------------------------------
        //change lower buttons depending on page

          {switch (currentPage) {
           | League(_) => button_Home
           | Team(_) => <> button_Home button_League </>
           | _AnyOtherPage => React.null
           }}
        </Box>
    </Box>;
};
