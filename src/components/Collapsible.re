open Images;

type state = {
  buttonText: string,
  showContent: bool,
};

type action =
  | ToggleContent(bool);

[@react.component]
let make = /*props*/
    (
      ~background: string="linear-gradient(to bottom right, rgb(73, 70, 80), rgb(83, 80, 90))",
      ~border: string="solid #eee",
      ~borderWidth: string="0 0 0 3px",
      ~buttonFontSize: string="1.1rem",
      ~buttonPadding: string="0.85rem 0.95rem",
      ~buttonText_Color: string="#eee",
      ~buttonText_Closed: string,
      ~buttonText_Opened: string,
      ~className: string="",
      ~contentPadding: string="",
      ~justifyButtonText: string="center",
      ~onClick: unit => unit=() => (),
      ~onClickAnimation: bool=true,
      ~margin: string="0.5rem auto",
      ~showContent: bool=false,
      ~text: string="",
      ~textColor: string="#eee",
      ~textMargin: string="1rem",
      ~width: string="92%",
      ~children: React.element=React.null,
    ) => {
  //-----------------------------------
  //-------------------start of reducer
  let (state, dispatch) =
    React.useReducer(
      (_state, action) =>
        switch (action) {
        | ToggleContent(showContent) =>
          showContent == true
            ? {buttonText: buttonText_Closed, showContent: false}
            : {buttonText: buttonText_Opened, showContent: true}
        },
      //initial state
      {
        buttonText: {
          showContent == true ? buttonText_Opened : buttonText_Closed;
        },
        showContent,
        /*get initial value of state.showContent from the showContent prop, hence the redundancy*/
      },
    );
  //-----------------------------------
  //---------------------end of reducer

  //shorthand
  let showContent: bool = state.showContent;

  //----------------------------------------
  //----------------------------------layout
  <Box
    className={className ++ " collapsible"}
    flexDirection="column"
    margin
    width>
    <Button
      background
      color=buttonText_Color
      fontSize=buttonFontSize
      justifyContent=justifyButtonText
      onClick={() =>
        dispatch(
          showContent == false ? ToggleContent(false) : ToggleContent(true),
        )
        |> /*execute function defined by onClick prop after the dispatch*/ onClick
      }
      onClickAnimation
      padding=buttonPadding
      width="100%">
      //button text

        <div style={ReactDOMRe.Style.make(~color="inherit", ())}>
          {state.buttonText |> React.string}
        </div>
        //up-down arrow
        <img
          alt={showContent ? "arrow_up" : "arrow_down"}
          src={showContent ? chevronUp : chevronDown}
          style={ReactDOMRe.Style.make(~marginLeft="0.3rem", ())}
        />
      </Button>
    //content
    <div
      style={ReactDOMRe.Style.make(
        ~background="rgba(53, 50, 59, 0.2)",
        ~border,
        ~borderWidth,
        ~boxShadow="0 15px 38px -27px inset black",
        ~color=textColor,
        ~lineHeight="1.65",
        ~margin={showContent ? "0.4rem auto 0.2rem" : "0"},
        ~maxHeight={showContent ? "500vh" : "0"},
        ~opacity={showContent ? "1" : "0"},
        ~overflow="hidden",
        ~padding=contentPadding,
        ~pointerEvents={showContent ? "auto" : "none"},
        ~transition="300ms, padding 150ms",
        ~width="99%",
        (),
      )}>
      //---------------------------------------------------
      //use children prop instead of text prop if you want to insert more than a paragraph
      //if children are present, the text prop will not show up

        {children == React.null
           ? React.null : <div className="collapsible_content"> children </div>}
        //--------------------------------------------------------
        //text prop: if we just want to insert a paragraph of text & choose not to use children
        //REMINDER: if children are present, the text prop will not show up
        {children == React.null
           ? <p
               className="collapsible_text"
               style={ReactDOMRe.Style.make(~margin=textMargin, ())}>
               {text |> React.string}
             </p>
           : React.null}
      </div>
  </Box>;
};
