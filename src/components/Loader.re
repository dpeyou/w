open Helpers;

[@react.component]
let make = /*props*/
    (~text: string="loading stuff...") => {
  //------------------------------
  //------------------------layout
  <Box
    className="loader"
    flexDirection="column"
    height="100%"
    justifyContent="center"
    position="absolute">
    <Box
      className="loaderText"
      color="lightyellow"
      fontSize="1.5rem"
      margin="0 0 2rem">
      {text |> str}
    </Box>
    //background="linear-gradient(to left, #ffa0a0, #ffe0a0"
    <Button
      border="#eee dashed"
      borderWidth="3px"
      className="loaderButton button_Home"
      onClick={() => push("/home/")}
      // color="#333"
      padding="1rem"
      width="50%">
      {"If this takes way too long, click here to return to home" |> str}
    </Button>
  </Box>;
};
