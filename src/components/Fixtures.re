open Fallback;
open Helpers;
open Types;

[@react.component]
let make = /*props*/
    (
      ~clickable: bool=false /*Decide on whether we can navigate to teams we click on*/ /*Clickable on League page but not on Team page*/,
      ~clickTeam: (string, unit) => unit=(_teamId, ()) => (),
      ~fixtures: list(fixture),
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~scrollTop: int=0,
      ~team: team=defaultTeam,
      ~text: string="US pacific time (UTC-07) & 24hr clock in use",
    ) => {
  //----------------------------------------
  //----------------------------------layout
  <ScrollView
    id={
      /*alternate ids based on the current page*/
      /*check whether a team was set*/ team == defaultTeam
        ? "ScrollView_Fixtures" : "ScrollView_Next5"
    }
    onScroll
    scrollTop>
    <p
      className="text"
      style={ReactDOMRe.Style.make(
        ~color="lightyellow",
        ~margin="2.5rem 2rem 2rem",
        ~textAlign="center",
        (),
      )}>
      {text |> str}
    </p>
    {fixtures
     |> Array.of_list
     |> Array.map((fixture: fixture) =>
          <Fixture clickable clickTeam fixture team />
        )
     |> React.array}
  </ScrollView>;
};
