open Images;
open Types;

let twitter: team => mediaLink =
  team => {
    color: "#bbf",
    logo: twitterLogo,
    text: "twitter",
    url: {
      team.twitter;
    },
  };

let website: team => mediaLink =
  team => {
    color: "#eee",
    logo: websiteLogo,
    text: "website",
    url: {
      team.website;
    },
  };

let youtube: team => mediaLink =
  team => {
    color: "#fbb",
    logo: youtubeLogo,
    text: "youtube",
    url: {
      team.youtube;
    },
  };

let generateMediaLinks: team => array(mediaLink) =
  team => [|twitter(team), website(team), youtube(team)|];

[@react.component]
let make = /*props*/
    (~team: team) =>
  //----------------------------------
  //----------------------------layout
  <Box
    className="mediaLinks"
    justifyContent="space-around"
    margin="2rem auto 2.75rem"
    width="88%">
    {team
     |> generateMediaLinks
     |> Array.map((mediaLink: mediaLink)
          //only display element if the media url exists
          =>
            switch (mediaLink.url) {
            //check for empty string
            | Some("") => React.null
            | Some(url) =>
              <a
                className="mediaLink"
                href={"https://" ++ url}
                key={mediaLink.text}
                style={ReactDOMRe.Style.make(
                  ~alignItems="center",
                  ~background=
                    "linear-gradient(to left, rgb(73, 70, 80), rgb(83, 80, 90))",
                  ~border="solid " ++ mediaLink.color,
                  ~borderRadius="3%",
                  ~borderWidth="3px 0 0 0",
                  ~color=mediaLink.color,
                  ~display="flex",
                  ~flexDirection="column",
                  ~padding="1rem",
                  ~textDecoration="none",
                  ~width="5rem",
                  (),
                )}
                target="_blank">
                <img
                  alt="media_url"
                  src={mediaLink.logo}
                  style={ReactDOMRe.Style.make(
                    ~marginBottom="0.2rem",
                    ~width="1.8rem",
                    (),
                  )}
                />
                {mediaLink.text |> React.string}
              </a>
            | None => React.null
            }
          )
     |> React.array}
  </Box>;
