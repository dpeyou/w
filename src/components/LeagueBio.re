open Types;

[@react.component]
let make = /*props*/
    (
      ~league: league,
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~scrollTop: int=0,
    ) => {
  <ScrollView id="ScrollView_LeagueBio" onScroll scrollTop>
    //------
    //banner

      <img
        alt="league_banner"
        src={league.banner}
        style={ReactDOMRe.Style.make(
          //~borderBottom="solid 4px #dde",
          ~paddingTop="2.6rem",
          ~width="100%",
          (),
        )}
      />
      //-------
      //fan art
      <img
        alt="fan_art"
        src={league.fanArt}
        style={ReactDOMRe.Style.make(
          ~borderBottom="solid 4px #333",
          ~width="100%",
          (),
        )}
      />
      <Box alignItems="center" flexDirection="column" padding="1.0rem 0 10rem">
        //----------
        //league bio

          <Collapsible
            buttonText_Closed="About league"
            buttonText_Opened="About league"
            className="aboutLeague"
            showContent=true
            text={league.description}
            textMargin="1rem"
          />
        </Box>
    </ScrollView>;
};
