open Helpers;
open Types;

let tableCells: ranking => array(tableCell) =
  ranking => [|
    {color: "#eee", content: ranking.gamesPlayed},
    {color: "#b0c0ff", content: ranking.points},
    {color: "#d0ffc0", content: ranking.wins},
    {color: "#ffa0a0", content: ranking.losses},
    {color: "#ffe0a0", content: ranking.draws},
  |];

let tableHeaders: array(tableHeader) = [|
  {color: "#ffe0a0", padding: "0.3rem 0.8rem", text: "Team"},
  {color: "#eee", padding: "0 0.25rem", text: "MP"},
  {color: "#b0c0ff", padding: "0 0.25rem", text: "Pts"},
  {color: "#d0ffc0", padding: "0 0.4rem", text: "W"},
  {color: "#ffa0a0", padding: "0 0.7rem", text: "L"},
  {color: "#ffe0a0", padding: "0 0.6rem", text: "D"},
|];

[@react.component]
let make = /*props*/
    (
      ~clickTeam: (ranking, unit) => unit,
      ~league: league,
      ~onScroll: (int, unit) => unit=(_, ()) => (),
      ~scrollTop: int=0,
      ~table: list(ranking),
    ) => {
  //----------------------------------------
  //----------------------------------layout
  //check whether the table has been loaded
  table == []
    ? <Loader text="Loading league table..." />
    : <div id="TableTab">
        <ScrollView id="ScrollView_Table" onScroll scrollTop>
          <p
            style={ReactDOMRe.Style.make(
              ~color="lightyellow",
              ~lineHeight="1.3",
              ~margin="2.5rem 2rem 2rem",
              ~textAlign="center",
              (),
            )}>
            {"Click a team to navigate to it" |> str}
          </p>
          <table
            id="leagueTable"
            style={ReactDOMRe.Style.make(
              ~padding="0 0.75rem 2rem",
              ~width="100%",
              (),
            )}>
            //-----------------------------------
            //--------------------table heading

              <thead>
                <tr
                  style={ReactDOMRe.Style.make(
                    ~background="#4a4a55",
                    ~position="initial",
                    (),
                  )}>
                  //horizontal headers

                    {tableHeaders
                     |> Array.map((header: tableHeader) =>
                          <th
                            key={header.text}
                            style={ReactDOMRe.Style.make(
                              ~color=header.color,
                              ~padding=header.padding,
                              (),
                            )}>
                            {header.text |> str}
                          </th>
                        )
                     |> React.array}
                  </tr>
              </thead>
              //-------------------------------
              //------------------table body
              <tbody>
                {table
                 |> Array.of_list
                 |> Array.mapi((index, ranking: ranking) =>
                      <tr key={index |> string_of_int}>
                        //---------------------------------
                        //----team name (button container)

                          <th
                            className="team"
                            style={ReactDOMRe.Style.make(
                              ~textAlign="left",
                              (),
                            )}>
                            <Button
                              borderRadius="0"
                              color="#eef"
                              display="inline-block"
                              onClick={() => clickTeam(ranking, ())}
                              padding="0.5rem"
                              textAlign="left"
                              transform="none"
                              width="100%">
                              <div className="teamIndex inlineB yellowText">
                                {index + 1 |> string_of_int |> str}
                              </div>
                              {". " |> str}
                              {ranking.teamName |> str}
                            </Button>
                          </th>
                          {ranking
                           |> tableCells
                           |> Array.mapi((index2, cell: tableCell) =>
                                <td
                                  key={index2 |> string_of_int}
                                  style={ReactDOMRe.Style.make(
                                    ~color=cell.color,
                                    ~textAlign="center",
                                    (),
                                  )}>
                                  {cell.content |> string_of_int |> str}
                                </td>
                              )
                           |> React.array}
                        </tr>
                    )
                 |> React.array}
              </tbody>
            </table>
        </ScrollView>
        <Navigation leagueId={league.id} />
      </div>;
};
