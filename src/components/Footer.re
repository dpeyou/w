[@react.component]
let make = /*props*/
    (
      ~background: string="none",
      ~boxShadow: string="0 0 103px 3px #1e0a0c",
      ~display: string="flex",
      ~height: string="",
      ~id: string="",
      ~flexDirection: string="column",
      ~fontFamily: string="",
      ~padding: string="0 6%",
      ~position: string="absolute",
      ~children: React.element,
    ) => {
  //---------------------------------
  //---------------------------layout
  <footer
    className="footer"
    id
    style={ReactDOMRe.Style.make(
      ~background,
      ~bottom="0",
      ~boxShadow,
      ~display,
      ~fontFamily,
      ~fontSize="1.2rem",
      ~height,
      ~left="0",
      ~padding,
      ~position,
      ~right="0",
      ~transition="0s",
      ~zIndex="1",
      (),
    )}>
    <div
      className="wrapper"
      style={ReactDOMRe.Style.make(
        ~alignItems="center",
        ~display="flex",
        ~height="100%",
        ~flexDirection,
        ~justifyContent="space-between",
        ~whiteSpace="pre-line",
        ~width="100%",
        (),
      )}>
      children
    </div>
  </footer>;
};
