open Types;

//-----------------------------------------------------
//to add a league, start by creating a new leagueButton
let bundesliga = {
  country: "Germany",
  id: "4331",
  name: "Bundesliga",
  sport: Football,
};

let laliga = {country: "Spain", id: "4335", name: "La liga", sport: Football};

let ligue1 = {
  country: "France",
  id: "4334",
  name: "Ligue 1 de France",
  sport: Football,
};

let nwsl = {
  country: "USA",
  id: "4521",
  name: "National Women's Soccer League",
  sport: Football,
};

let premierleague = {
  country: "England",
  id: "4328",
  name: "English Premier League",
  sport: Football,
};

//-------------------------------
//array/list of all leagueButtons
let leagueButtons: array(leagueButton) = [|
  bundesliga,
  laliga,
  ligue1,
  nwsl,
  premierleague,
|];
