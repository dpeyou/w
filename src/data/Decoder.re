open Fallback;
open Json.Decode;
open Types;

//javascript promise
let fetchData =
    (
      ~api_string: string => string,
      ~fallbackValue: 'a,
      ~jsonDecoder: Json.Decode.decoder(list(option('a))),
      ~targetId: string,
    ) =>
  Js.Promise.(
    Fetch.fetch(targetId |> api_string)
    |> then_(Fetch.Response.json)
    |> then_(json => jsonDecoder(json) |> resolve)
    |> then_((response: list(option('a))) =>
         response
         |> List.filter((optionalItem: option('a)) =>
              switch (optionalItem) {
              | Some(_) => true
              | None => false
              }
            )
         //change items from optional to a regular record
         |> List.map((optionalItem: option('a)) =>
              switch (optionalItem) {
              | Some(item) => item
              | None =>
                /*return something of type 'a*/
                fallbackValue
              }
            )
         |> resolve
       )
  );

let leagueScheduleApi: string => string =
  leagueId =>
    "https://www.thesportsdb.com/api/v1/json/1/eventsnextleague.php?id="
    ++ leagueId;

let decodeLeagueSchedule: Json.Decode.decoder(list(option(fixture))) =
  field(
    "events",
    list(
      optional(json =>
        {
          awayTeamName: field("strAwayTeam", string, json),
          awayTeamId: field("idAwayTeam", string, json),
          date: field("dateEvent", string, json),
          homeTeamName: field("strHomeTeam", string, json),
          homeTeamId: field("idHomeTeam", string, json),
          kickoff: field("strTime", string, json),
          league: field("strLeague", string, json),
          leagueId: field("idLeague", string, json),
          season: field("strSeason", string, json),
          sport: field("strSport", string, json),
        }
      ),
    ),
  );

let getLeagueSchedule: string => Js.Promise.t(list(fixture)) =
  leagueId =>
    fetchData(
      ~api_string=leagueScheduleApi,
      ~fallbackValue={
        defaultFixture;
      },
      ~jsonDecoder=decodeLeagueSchedule,
      ~targetId=leagueId,
    );

//------------------------------------------------
//-------------------------get league-wide results
let leagueResultsApi: string => string =
  leagueId =>
    "
  https://www.thesportsdb.com/api/v1/json/1/eventspastleague.php?id="
    ++ leagueId;

let decodeLeagueResults: Json.Decode.decoder(list(option(result))) =
  field(
    "events",
    list(
      optional(json =>
        {
          awayTeamName: field("strAwayTeam", string, json),
          awayTeamId: field("idAwayTeam", string, json),
          awayTeamScore: field("intAwayScore", string, json),
          awayTeamScore_Details: field("strAwayGoalDetails", string, json),
          date: field("dateEvent", string, json),
          homeTeamName: field("strHomeTeam", string, json),
          homeTeamId: field("idHomeTeam", string, json),
          homeTeamScore: field("intHomeScore", string, json),
          homeTeamScore_Details: field("strHomeGoalDetails", string, json),
          league: field("strLeague", string, json),
          leagueId: field("idLeague", string, json),
          season: field("strSeason", string, json),
          sport: field("strSport", string, json),
        }
      ),
    ),
  );

let getLeagueResults: string => Js.Promise.t(list(result)) =
  leagueId =>
    fetchData(
      ~api_string=leagueResultsApi,
      ~fallbackValue={
        defaultResult;
      },
      ~jsonDecoder=decodeLeagueResults,
      ~targetId=leagueId,
    );

//---------------------------------------------------------
//---------------------------------------------league table
let leagueTableApi: (string, string) => string =
  (leagueId, seasonNumber) =>
    "https://www.thesportsdb.com/api/v1/json/1/lookuptable.php?l="
    ++ leagueId
    ++ "&s="
    ++ seasonNumber;

let decodeLeagueTable: Json.Decode.decoder(list(option(ranking))) =
  field(
    "table",
    list(
      optional(json =>
        {
          draws: field("draw", int, json),
          gamesPlayed: field("played", int, json),
          goals: field("goalsfor", int, json),
          goalsConceded: field("goalsagainst", int, json),
          goalDifference: field("goalsdifference", int, json),
          losses: field("loss", int, json),
          points: field("total", int, json),
          teamName: field("name", string, json),
          teamId: field("teamid", string, json),
          wins: field("win", int, json),
        }
      ),
    ),
  );

let getLeagueTable: (string, string) => Js.Promise.t(list(ranking)) =
  /*not exactly sure why I had to switch positions of seasonNumber & leagueId (compared to leagueTableApi above). Discovered this worked as intended through trial & error*/
  (seasonNumber, leagueId) =>
    fetchData(
      ~api_string=leagueTableApi(seasonNumber),
      ~fallbackValue=defaultRanking,
      ~jsonDecoder=decodeLeagueTable,
      ~targetId=leagueId,
    );

//---------------------------------------------------------
//-------------------------------------list of all seasons
let seasonListApi: string => string =
  leagueId =>
    "https://www.thesportsdb.com/api/v1/json/1/search_all_seasons.php?id="
    ++ leagueId;

let deCodeSeason: Json.Decode.decoder(list(option(season))) =
  field(
    "seasons",
    list(optional(json => {year: field("strSeason", string, json)})),
  );

let getSeasons: string => Js.Promise.t(list(season)) =
  leagueId =>
    fetchData(
      ~api_string=seasonListApi,
      ~fallbackValue={year: "n/a"},
      ~jsonDecoder=deCodeSeason,
      ~targetId=leagueId,
    );

//---------------------------------------------------------
//---------------------------------------------league bio
let leagueBioApi: string => string =
  leagueId =>
    "
    https://www.thesportsdb.com/api/v1/json/1/lookupleague.php?id="
    ++ leagueId;

let decodeLeagueBio: Json.Decode.decoder(list(option(league))) = {
  field(
    "leagues",
    list(
      optional(json =>
        {
          banner: field("strBanner", string, json),
          country: field("strDescriptionEN", string, json),
          description: field("strDescriptionEN", string, json),
          fanArt: field("strFanart1", string, json),
          id: field("idLeague", string, json),
          logo: field("strBadge", string, json),
          name: field("strLeague", string, json),
          twitter: field("strTwitter", optional(string), json),
          website: field("strWebsite", optional(string), json),
          youtube: field("strYoutube", optional(string), json),
        }
      ),
    ),
  );
};

let getLeagueBio: string => Js.Promise.t(list(league)) =
  leagueId =>
    fetchData(
      ~api_string=leagueBioApi,
      ~fallbackValue=defaultLeague,
      ~jsonDecoder=decodeLeagueBio,
      ~targetId=leagueId,
    );

//--------------------------------------------
//---------------------------------team next 5
let next5api = teamId =>
  "https://www.thesportsdb.com/api/v1/json/1/eventsnext.php?id=" ++ teamId;

let decodeNext5: Json.Decode.decoder(list(option(fixture))) =
  field(
    "events",
    list(
      optional(json =>
        {
          awayTeamName: field("strAwayTeam", string, json),
          awayTeamId: field("idAwayTeam", string, json),
          date: field("dateEvent", string, json),
          homeTeamName: field("strHomeTeam", string, json),
          homeTeamId: field("idHomeTeam", string, json),
          kickoff: field("strTime", string, json),
          league: field("strLeague", string, json),
          leagueId: field("idLeague", string, json),
          season: field("strSeason", string, json),
          sport: field("strSport", string, json),
        }
      ),
    ),
  );

let getNext5: string => Js.Promise.t(list(fixture)) =
  teamId =>
    fetchData(
      ~api_string=next5api,
      ~fallbackValue=defaultFixture,
      ~jsonDecoder=decodeNext5,
      ~targetId=teamId,
    );

//-------------------------------------------
//-------------------------------------last 5
let last5api = teamId =>
  "https://www.thesportsdb.com/api/v1/json/1/eventslast.php?id=" ++ teamId;

let decodeLast5: Json.Decode.decoder(list(option(result))) =
  field(
    "results",
    list(
      optional(json =>
        {
          awayTeamName: field("strAwayTeam", string, json),
          awayTeamId: field("idAwayTeam", string, json),
          awayTeamScore: field("intAwayScore", string, json),
          awayTeamScore_Details: field("strAwayGoalDetails", string, json),
          date: field("dateEvent", string, json),
          homeTeamName: field("strHomeTeam", string, json),
          homeTeamId: field("idHomeTeam", string, json),
          homeTeamScore: field("intHomeScore", string, json),
          homeTeamScore_Details: field("strHomeGoalDetails", string, json),
          league: field("strLeague", string, json),
          leagueId: field("idLeague", string, json),
          season: field("strSeason", string, json),
          sport: field("strSport", string, json),
        }
      ),
    ),
  );

let getLast5: string => Js.Promise.t(list(result)) =
  teamId =>
    fetchData(
      ~api_string=last5api,
      ~fallbackValue=defaultResult,
      ~jsonDecoder=decodeLast5,
      ~targetId=teamId,
    );

//-----------------------------------------------------
//--------------------------------------fetch team bio
let teamBioApi: string => string =
  teamId =>
    "https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id=" ++ teamId;

let decodeTeamBio: Json.Decode.decoder(list(option(team))) =
  field(
    "teams",
    list(
      optional(json =>
        {
          badge: field("strTeamBadge", optional(string), json),
          banner: field("strTeamFanart1", optional(string), json),
          description: field("strDescriptionEN", optional(string), json),
          id: field("idTeam", string, json),
          leagueId: field("idLeague", string, json),
          location: field("strStadiumLocation", optional(string), json),
          name: field("strTeam", string, json),
          stadium: field("strStadium", optional(string), json),
          twitter: field("strTwitter", optional(string), json),
          website: field("strWebsite", optional(string), json),
          youtube: field("strYoutube", optional(string), json),
        }
      ),
    ),
  );

let getTeamBio: string => Js.Promise.t(list(team)) =
  teamId =>
    fetchData(
      /*Schedule.fetchData. Can be used with type team*/
      ~api_string=teamBioApi,
      ~fallbackValue=defaultTeam,
      ~jsonDecoder=decodeTeamBio,
      ~targetId=teamId,
    );
