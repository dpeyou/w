open Types;

let defaultFixture: fixture = {
  awayTeamName: "n/a",
  awayTeamId: "n/a",
  date: "xxxx-xx-xx",
  homeTeamName: "n/a",
  homeTeamId: "n/a",
  kickoff: "n/a",
  league: "n/a",
  leagueId: "n/a",
  season: "n/a",
  sport: "n/a",
};

let defaultLeague: league = {
  banner: "banner unavailable",
  country: "n/a",
  description: "n/a",
  fanArt: "",
  id: "",
  name: "Some league...",
  logo: "",
  twitter: Some(""),
  website: Some(""),
  youtube: Some(""),
};

let defaultResult: result = {
  awayTeamName: "",
  awayTeamId: "",
  awayTeamScore: "",
  awayTeamScore_Details: "",
  date: "",
  homeTeamName: "",
  homeTeamId: "",
  homeTeamScore: "",
  homeTeamScore_Details: "",
  league: "",
  leagueId: "",
  season: "",
  sport: "",
};

let defaultRanking: ranking = {
  draws: 0,
  gamesPlayed: 0,
  goals: 0,
  goalsConceded: 0,
  goalDifference: 0,
  losses: 0,
  points: 0,
  teamName: "n/a",
  teamId: "n/a",
  wins: 0,
};

let defaultTeam: team = {
  badge: Some(""),
  banner: Some(""),
  description: Some(""),
  id: "",
  leagueId: "",
  location: Some(""),
  name: "Name Unavailable",
  stadium: Some(""),
  twitter: Some(""),
  website: Some(""),
  youtube: Some(""),
};
