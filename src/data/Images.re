//collapsible component
[@bs.module] external chevronDown: string = "../img/chevronDown.svg";
[@bs.module] external chevronUp: string = "../img/chevronUp.svg";

//league, team pages
[@bs.module] external twitterLogo: string = "../img/twitter.svg";
[@bs.module] external websiteLogo: string = "../img/website.svg";
[@bs.module] external youtubeLogo: string = "../img/youtube.svg";

//home page
[@bs.module] external cameroon: string = "../img/cmr.png";
[@bs.module] external football: string = "../img/football.svg";
[@bs.module] external football2: string = "../img/football2.svg";
[@bs.module] external football3: string = "../img/football3.svg";
