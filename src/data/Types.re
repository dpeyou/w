type fixture = {
  awayTeamName: string,
  awayTeamId: string,
  date: string,
  homeTeamName: string,
  homeTeamId: string,
  kickoff: string,
  league: string,
  leagueId: string,
  season: string,
  sport: string,
};

type league = {
  banner: string,
  country: string,
  description: string,
  fanArt: string,
  id: string,
  logo: string,
  name: string,
  twitter: option(string),
  website: option(string),
  youtube: option(string),
};

type leagueButton = {
  country: string,
  id: string,
  name: string,
  sport,
}
and sport =
  | Basketball
  | Football;

type mediaLink = {
  color: string,
  logo: string,
  text: string,
  url: option(string),
};

type menuButton = {
  onClick: unit => unit,
  text: string,
  trigger: bool,
};

type page =
  | Error
  | Home
  | League(string /*leagueId*/, string /*url.hash*/)
  | Team(string /*teamId*/, string /*url.hash*/);

//for league table
type ranking = {
  draws: int,
  gamesPlayed: int,
  goals: int,
  goalsConceded: int,
  goalDifference: int,
  losses: int,
  points: int,
  teamName: string,
  teamId: string,
  wins: int,
};

type result = {
  awayTeamName: string,
  awayTeamId: string,
  awayTeamScore: string,
  awayTeamScore_Details: string,
  date: string,
  homeTeamId: string,
  homeTeamName: string,
  homeTeamScore: string,
  homeTeamScore_Details: string,
  league: string,
  leagueId: string,
  season: string,
  sport: string,
};

type season = {year: string};

//use this in order to capture the state of items e.g collapsibles (opened, closed, etc) when navigating between tabs in a page
type tab =
  | Fixtures
  | LeagueBio
  | NoTab
  | Results
  | Table
  | TeamBio;

type tableCell = {
  color: string,
  content: int,
};

type tableHeader = {
  color: string,
  padding: string,
  text: string,
};

type team = {
  badge: option(string),
  banner: option(string),
  description: option(string),
  id: string,
  leagueId: string,
  location: option(string),
  name: string,
  stadium: option(string),
  twitter: option(string),
  website: option(string),
  youtube: option(string),
};

type url = ReasonReact.Router.url;
