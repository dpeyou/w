open Types;

let getElementById: string => React.element =
  _id => [%bs.raw {|
  document.getElementById(_id)
|}];

let hashToTab: string => tab =
  urlHash =>
    switch (urlHash) {
    | "fixtures" => Fixtures
    | "leagueBio" => LeagueBio
    | "results" => Results
    | "table" => Table
    | "teamBio" => TeamBio
    | _ => NoTab
    };

//pattern-match leagueId to some string
let idToLeagueName: string => string =
  id =>
    switch (id) {
    | "4331" => "Bundesliga"
    | "4335" => "La Liga"
    | "4334" => "Ligue 1"
    | "4521" => "NWSL"
    | "4328" => "Premier League"
    | _ => "League name unavailable"
    };

let nothingHappens: unit => unit = () => ();

//for scroll-to-top button
//for capturing the corresponding scrollTop value
let pageToScrollViewId: page => string =
  page =>
    switch (page) {
    | Home => "ScrollView_Home"
    | League(_, urlHash) =>
      switch (urlHash) {
      | "fixtures" => "ScrollView_Fixtures"
      | "leagueBio" => "ScrollView_LeagueBio"
      | "results" => "ScrollView_Results"
      | "table" => "ScrollView_Table"
      | _ => ""
      }
    | Team(_, urlHash) =>
      switch (urlHash) {
      | "teamBio" => "ScrollView_TeamBio"
      | "last5" => "ScrollView_Last5"
      | "next5" => "ScrollView_Next5"
      | _ => ""
      }
    | _ => ""
    };

let push: string => unit = ReasonReact.Router.push;

//return the latest season from the fetched list of seasons
//needed for <League/> & <Team/> components to load (league table needs latest season)
let returnLatestSeason: list(season) => string =
  seasons => {
    seasons
    |> Array.of_list
    |> (
      seasons =>
        /*get last item in array by getting its length, then subtracting by 1 since array indices start with zero*/
        seasons[Array.length(seasons) - 1]
        //return the string we want
        |> (season => season.year)
    );
  };

//for scroll-to-top button. When clicked, set scrollTop value of target(string) to zero
let scrollToTop: string => unit =
  _id => [%bs.raw
    {|
            document.getElementById(_id).scrollTop = 0
          |}
  ];

let str: string => React.element = React.string;

//convert time to Pacific time
let toPacificTime: string => string =
  time => {
    let gmtHours: int = String.sub(time, 0, 2) |> int_of_string;
    //using 24-hour clock: modulus = 24
    let pacificHours: string = gmtHours - 7 mod 24 |> string_of_int;
    //minutes don't change, fortunately
    let minutes: string = String.sub(time, 3, 2);
    let pacifiTime: string = pacificHours ++ ":" ++ minutes;
    //return pacific time
    pacifiTime;
  };

//pattern-match url to a page
let urlToPage: url => page =
  url =>
    switch (url.path, url.hash) {
    | ([""], _)
    | (["home"], _) => Home
    | (["league", leagueId], urlHash) => League(leagueId, urlHash)
    | (["team", teamId], urlHash) => Team(teamId, urlHash)
    //for now, send back to Home page instead of using Error page
    /*| _ => Error*/
    | _AnyOtherRoute => Home
    };
