open Decoder;
open Helpers;
open LeagueButtons;
open Types;

module App = {
  type state = {
    scrollTop_Home: int,
    scrollTop_TeamBio: int,
    season_Bundesliga: string,
    season_Laliga: string,
    season_Ligue1: string,
    season_NWSL: string,
    season_PremierLeague: string,
  };

  type action =
    | GetSeasons(string /*leagueId*/, list(season))
    | SetScrollTop(page, int);

  [@react.component]
  let make = /*no props*/ () => {
    //set the current page using router api
    //https://reasonml.github.io/reason-react/docs/en/router
    let currentPage: page =
      ReasonReact.Router.useUrl() |> /*pattern-match url to a page*/ urlToPage;
    //------------------------------------------
    //--------------------------start of reducer
    let (state, dispatch) =
      React.useReducer(
        (state, action) =>
          switch (action) {
          | GetSeasons(leagueId, seasons) =>
            switch (leagueId) {
            | "4331" /*bundesliga*/ => {
                ...state,
                season_Bundesliga: seasons |> returnLatestSeason,
              }
            | "4335" /*laliga*/ => {
                ...state,
                season_Laliga: seasons |> returnLatestSeason,
              }
            | "4334" /*ligue1*/ => {
                ...state,
                season_Ligue1: seasons |> returnLatestSeason,
              }
            | "4521" /*nwsl*/ => {
                ...state,
                season_NWSL: seasons |> returnLatestSeason,
              }
            | "4328" /*premier league*/ => {
                ...state,
                season_PremierLeague: seasons |> returnLatestSeason,
              }
            | _ => state
            }

          | SetScrollTop(page, scrollTop) =>
            switch (page) {
            | Home => {...state, scrollTop_Home: scrollTop}
            | Team(_, urlHash) =>
              urlHash == "teamBio"
                ? {...state, scrollTop_TeamBio: scrollTop} : state
            | _ => state
            }
          },
        //initial state
        {
          scrollTop_Home: 0,
          scrollTop_TeamBio: 0,
          season_Bundesliga: "",
          season_Laliga: "",
          season_Ligue1: "",
          season_NWSL: "",
          season_PremierLeague: "",
        },
      );
    //-----------------------------------------
    //--------------------------end of reducer

    let idToSeason: string => string =
      leagueId =>
        switch (leagueId) {
        | "4331" => state.season_Bundesliga
        | "4335" => state.season_Laliga
        | "4334" => state.season_Ligue1
        | "4521" => state.season_NWSL
        | "4328" => state.season_PremierLeague
        | _ => ""
        };

    let currentSeason: string =
      switch (currentPage) {
      | League(leagueId, _) => leagueId |> idToSeason
      | _ => ""
      };

    //get season number list, so that we can retrieve the latest season. Latest season needed to load league table
    let fetchSeasons: string => unit =
      leagueId =>
        getSeasons(leagueId)
        |> Js.Promise.then_((response: list(season)) => {
             //send to state
             dispatch(GetSeasons(leagueId, response));
             Js.Promise.resolve();
           })
        |> ignore;

    let _fetchAllLeagueSeasons = {
      React.useState(() => {
        fetchSeasons(bundesliga.id);
        fetchSeasons(laliga.id);
        fetchSeasons(ligue1.id);
        fetchSeasons(nwsl.id);
        fetchSeasons(premierleague.id);
      });
    };

    //-----------------------------------------
    //-----------------------------------layout
    <div
      id="App"
      style={ReactDOMRe.Style.make(
        ~background=
          "linear-gradient(to bottom right, #555a60, #62606a, #68656f)",
        ~bottom="0",
        ~left="0",
        ~margin="0 auto",
        ~maxWidth="700px",
        ~overflow="hidden",
        ~position="absolute",
        ~right="0",
        ~top="0",
        (),
      )}>
      //------------
      //pages/routes

        {switch (currentPage) {
         | Home =>
           <Home
             navToLeague={leagueId =>
               push("/league/" ++ leagueId ++ "#table")
             }
             onScroll={(scrollTop, ()) =>
               dispatch(SetScrollTop(Home, scrollTop))
             }
             scrollTop={state.scrollTop_Home}
           />

         | League(leagueId, urlHash) =>
           /*check whether the latest season has been loaded*/
           /*latest season needed in order to load league table*/
           currentSeason == /*empty string*/ ""
             ? <Loader text="Fetching latest season..." />
             : <League
                 currentSeason
                 leagueIdProp=leagueId
                 navToTeam={(teamId, ()) =>
                   push("/team/" ++ teamId ++ "#next5")
                 }
                 urlHash
               />

         | Team(teamId, urlHash) =>
           <Team
             onScroll={(scrollTop, ()) =>
               dispatch(SetScrollTop(Team(teamId, urlHash), scrollTop))
             }
             resetScrollTop={() =>
               dispatch(SetScrollTop(Team(teamId, urlHash), 0))
             }
             scrollTop={state.scrollTop_TeamBio}
             teamIdProp=teamId
             urlHash
           />
         //----------------
         //error page/route
         | _ => <div> {"error" |> str} </div>
         }}
        //--------------------
        //scroll-to-top button
        <ScrollToTop
          currentPage
          display={
            (
              switch (currentPage) {
              | Home => state.scrollTop_Home
              | Team(_, urlHash) =>
                urlHash == "teamBio" ? state.scrollTop_TeamBio : 0
              | _anyOtherPage => 0 /* 0 < 600, therefore item won't display in this case*/
              /*<League/> page has its own <ScrollToTop/> component. Should move it here for simplicity*/
              }
            )
            > 600
              ? "block" : "none"
          }
        />
      </div>;
  };
};
//render this app to HTML file at div with ID "App"
ReactDOMRe.renderToElementWithId(<App />, "AppContainer");
